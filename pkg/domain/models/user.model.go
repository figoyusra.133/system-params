package models

type User struct {
	BaseModel
	Email    string `gorm:"not null; unique"`
	Password string `gorm:"not null; unique"`
	Name     string `gorm:"name"`
}
