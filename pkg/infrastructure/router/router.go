package router

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"system/pkg/adapter/controllers"
)

func NewRouter(e *echo.Echo, c controllers.AppController) *echo.Echo {
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	//USERS
	e.GET("/users", func(context echo.Context) error { return c.User.GetUsers(context) })

	return e
}
