package configs

import (
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"gorm.io/gorm/schema"
	"system/pkg/domain/models"
)

func DataBase(cfg *Config) (*gorm.DB, error) {
	host := cfg.DBHost
	user := cfg.DBUser
	password := cfg.DBPassword
	dbName := cfg.DBName
	port := cfg.DBPort

	var logLevel logger.LogLevel

	if cfg.Logging == "true" {
		logLevel = logger.Info
	} else {
		logLevel = logger.Silent
	}

	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Asia/Jakarta", host, user, password, dbName, port)
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			SingularTable: true,
			NoLowerCase:   true,
			TablePrefix:   "ts_",
		},
		Logger: logger.Default.LogMode(logLevel),
	})

	err = db.AutoMigrate(&models.User{})

	if err != nil {
		return nil, err
	}

	return db, nil
}
