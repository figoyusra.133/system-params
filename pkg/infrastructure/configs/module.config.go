package configs

import (
	"github.com/go-playground/validator/v10"
	"github.com/joho/godotenv"
	"log"
	"os"
)

type Config struct {
	DBHost     string `validate:"required"`
	DBPort     string `validate:"required"`
	DBUser     string `validate:"required"`
	DBPassword string `validate:"required"`
	DBName     string `validate:"required"`
	Port       string `validate:"required"`
	Logging    string `validate:"required"`
}

var validate *validator.Validate

func ModuleConfig() *Config {
	err := godotenv.Load()

	if err != nil {
		log.Fatalf("Error loading .env file")
	}

	validate = validator.New()

	config := &Config{
		DBHost:     os.Getenv("DB_HOST"),
		DBPort:     os.Getenv("DB_PORT"),
		DBUser:     os.Getenv("DB_USER"),
		DBPassword: os.Getenv("DB_PASSWORD"),
		DBName:     os.Getenv("DB_NAME"),
		Port:       os.Getenv("PORT"),
		Logging:    os.Getenv("LOGGING"),
	}

	err = validate.Struct(config)

	if err != nil {
		log.Fatalf("Error validating environment variables: %s", err)
	}

	return config
}
