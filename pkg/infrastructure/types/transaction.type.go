package types

type TransactionInterface interface {
	Transaction(func(interface{}) (interface{}, error)) (interface{}, error)
}
