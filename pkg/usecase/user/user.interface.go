package user

import "system/pkg/domain/models"

type UserInterface interface {
	FindAll(u []*models.User) ([]*models.User, error)
}
