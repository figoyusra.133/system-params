package user

import (
	"system/pkg/domain/models"
	"system/pkg/infrastructure/types"
)

type userUsecase struct {
	userInterface UserInterface
	transaction   types.TransactionInterface
}

type User interface {
	List(u []*models.User) ([]*models.User, error)
}

func NewUserUsecase(i UserInterface, tx types.TransactionInterface) User {
	return &userUsecase{i, tx}
}

func (uu *userUsecase) List(u []*models.User) ([]*models.User, error) {
	u, err := uu.userInterface.FindAll(u)
	if err != nil {
		return nil, err
	}
	return u, nil
}
