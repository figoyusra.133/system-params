package repositories

import (
	"gorm.io/gorm"
	"system/pkg/domain/models"
	"system/pkg/usecase/user"
)

type UserRepository struct {
	db *gorm.DB
}

func NewUserRepository(db *gorm.DB) user.UserInterface {
	return &UserRepository{db}
}

func (ur *UserRepository) FindAll(u []*models.User) ([]*models.User, error) {
	err := ur.db.Find(&u).Error

	if err != nil {
		return nil, err
	}

	return u, nil
}
