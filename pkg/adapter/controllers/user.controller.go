package controllers

import (
	"net/http"
	"system/pkg/domain/models"
	"system/pkg/infrastructure/types"
	"system/pkg/usecase/user"
)

type userController struct {
	userUsecase user.User
}

type User interface {
	GetUsers(ctx types.Context) error
}

func NewUserController(us user.User) User {
	return &userController{us}
}

func (uc *userController) GetUsers(ctx types.Context) error {
	var u []*models.User

	u, err := uc.userUsecase.List(u)

	if err != nil {

	}

	return ctx.JSON(http.StatusOK, u)
}
