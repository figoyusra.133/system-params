package registry

import (
	"system/pkg/adapter/controllers"
	"system/pkg/adapter/repositories"
	"system/pkg/infrastructure/configs"
	"system/pkg/usecase/user"
)

func (r *registry) NewUserController() controllers.User {
	u := user.NewUserUsecase(
		repositories.NewUserRepository(r.db),
		configs.NewDBRepository(r.db),
	)

	return controllers.NewUserController(u)
}
