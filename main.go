package main

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"system/pkg/infrastructure/configs"
	"system/pkg/infrastructure/router"
	"system/pkg/registry"
)

func main() {
	e := echo.New()

	cfg := configs.ModuleConfig()

	db, err := configs.DataBase(cfg)

	if err != nil {
		e.Logger.Fatalf("Error initializing GORM: %s", err)
	}

	r := registry.NewRegistry(db)
	e = router.NewRouter(e, r.NewAppController())
	e.Logger.Fatal(e.Start(fmt.Sprintf(":%s", cfg.Port)))
}
